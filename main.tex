% documentclass{} takes the following mutually-exclusive options:
%
%  note      -- formats the document as a technical note.
%  proposed  -- formats the document as a proposed change to the baseline.
%  baseline  -- formats the document as part of the technical baseline.
\documentclass[note]{astron}

\input{meta}
\input{changes}

\setDocTitle{SDC Program Community Interactions}
\setDocProgram{SDC}
\setDocAuthors{
  \addPerson{John D. Swinbank}{ASTRON}{\vcsDate}
}

% These are set according to information obtained from make and git.
\setDocNumber{\docHandle}
\setDocRevision{\vcsRevision}
\setDocDate{\vcsDate}

\newcommand{\sdcCite}[1]{\citetitle{#1} \parencite{#1}}

\begin{document}
\maketitle

\printglossary[title=List of Abbreviations]
\clearpage

\section{Introduction}
\label{sec:intro}

This document describes the expectations around community interactions by ASTRON's \gls{SDCP} and its members.
Refer to \citetitle{SDC-006} \parencite{SDC-006} for background information on the overall structure of the \gls{SDC}, and to \citetitle{SDC-009} \parencite{SDC-009} for details on the relationship the \gls{SDCP} and \gls{SDCO}.
These documents take precedence over the summary sketch provided here.

This document is structured as follows:

\begin{itemize}

  \item{\Cref{sec:goals} provides a brief overview as to how the \gls{SDCP}'s development goals are agreed upon;}
  \item{\Cref{sec:broad} describes the interactions between the \gls{SDCO} team and the community, and are hence \emph{out} of scope for \gls{SDCP};}
  \item{\Cref{sec:tech} describes expectations around the various community interactions which are \emph{in} scope for the \gls{SDCP};}
  \item{\Cref{sec:public} describes various ways in which members of the community can monitor and understand the work which is being undertaken by the \gls{SDCP}.}

\end{itemize}

\section{Defining SDC Development Goals}
\label{sec:goals}

The \gls{SDCP} is primarily charged with software development in support of the \gls{SDC} Facility.
As described in \cref{sec:broad}, the overall development goals and priorities will be established by the \gls{SDCO} team in conjunction with the community.
However, we identify three specific areas of activity where it is appropriate for the \gls{SDCP} to independently initiate an development effort:

\begin{itemize}

  \item{The \gls{SDCP} is primarily funded through externally-sponsored projects.
        In agreeing to carry out these projects, ASTRON and the \gls{SDCP} have made commitments to the project sponsors to meet certain milestones and provide specific deliverables.
        Meeting these targets is an \gls{SDCP} responsibility which is not mediated by \gls{SDCO}, although it is hoped that careful planning will ensure that project and \gls{SDCO} goals are generally well-aligned.}

  \item{Internal technical considerations.
        The \gls{SDCP} is responsible for ensuring that the products delivered to \gls{SDCO} and to the wider community are robust, maintainable, and fit-for-purpose.
        It is appropriate for the \gls{SDCP} to schedule work on the technical underpinnings of its product offering which do not directly provide new capabilities or features to end users.}

  \item{Community collaboration.
        The \gls{SDCP} team will explicitly seek collaboration with external developers and other experts in an effort to engage them with our own efforts.
        We believe that by combining ASTRON's internal development effort with this external expertise, the best results can be achieved both for ASTRON and for the wider community.
        However, external parties will clearly bring their own ambitions and goals in addition to those advanced by ASTRON; our schedule and development effort must be flexible enough to accommodate these.}

\end{itemize}

Note that \emph{all} work carried out under the aegis of the \gls{SDCP} is ultimately subject to prioritization by the \gls{PMT} and approval by the Program Manager \parencite{SDC-002}.
This includes both community-driven work advanced by \gls{SDCO} \emph{and} work in all three of the areas described above.
The \gls{PMT} provides a forum for ensuring alignment of goals and priorities from these various disparate sources, and for breaking deadlocks if they should arise.

It is the last of the three bullet points above on which this document focuses: how can the \gls{SDCP} arrange its interactions with the community to best ensure a productive and harmonious relationship for all parties concerned?

\section{Broad Community Input and Prioritization}
\label{sec:broad}

The \gls{SDCO} team will put in place processes and procedures for soliciting and accepting feedback and suggestions from the broad scientific community on the capabilities of, and plans for, the \gls{SDC} Facility.
An exhaustive description of these processes is outside the scope of this document; however, they will include regular interactions with the Board of the \gls{ILT}; the \gls{LUC}; representatives of the various \gls{LOFAR} \glspl{KSP}; other relevant organizations representing sections of the scientific community; and with individual practising astronomers.
The \gls{SDCO} team will represent these various stakeholders as it works with members of the \gls{SDCP} to develop a prioritized development roadmap.
The mechanisms by which this roadmap will be developed are described in the \citetitle{SDC-002} \parencite{SDC-002}.

It follows from the above that the \gls{SDCP} is not, in general, expected to directly interact with the wider scientific community regarding the general prioritization of development.
By ensuring that \gls{SDCO} has full responsibility for community management of this sort, we both ensure that priorities and requests can be evaluated holistically --- a single organization has oversight of all community inputs --- and that messaging to the community is consistent.

It is understood and accepted --- indeed, encouraged --- that members of the \gls{SDCP} are also members of the scientific community, and will naturally discuss current and future Facility functionality with their peers and collaborators.
\gls{SDCP} members, when operating in this context, should understand that they have only a partial view of the prioritization process and that the development roadmap may be subject to change, and therefore avoid giving unrealistic or unsupported guarantees to members of the community.

\section{Technical Interactions with the Community}
\label{sec:tech}

As described in the \sdcCite{SDC-002}, the \gls{SDCP} has adopted a \gls{SAFe}-based approach to development.
As such, development is undertaken by agile teams, with a per-team Product Owner responsible for day-to-day prioritization of the work.
It is expected that technical interactions with the community will take place at the level of the development teams: this is where the technical and scientific expertise within the \gls{SDCP} is found.

We anticipate the community interactions may take one of three forms; they are addressed separately here.

\subsection{Full Team Membership}

A non-ASTRON employee may serve as a full member of the development team under certain circumstances.
Specifically, this would include:

\begin{enumerate}

  \item{If a non-ASTRON employee accepts funding or other support for a certain fraction of their time from \gls{SDCP} sources, then that person would normally be expected to act as a member of the development team for the part of their time which is covered.}

  \item{A non-ASTRON employee may enter into a memorandum of understanding in which they agree to act as a team member for some fraction of their time.}

\end{enumerate}

Under these circumstances, the individual concerned will act with the full rights and responsibilities of the other team members.
In particular, they will share in responsibility for the team's deliverables, will participate in team events, and will work under the guidance of the Product Owner to establish and execute the priorities and goals for the team's iterations and program increments.

\subsection{Organized Collaboration}
\label{sec:tech:organize}

In many circumstances, it is not appropriate for non-ASTRON employees to become full members of the development team.
Instead, we seek to foster productive collaboration and mutual understanding of goals with them.

The first point of contact in building such a relationship is the Product Owner of the relevant team.
Product Owners are empowered to identify and work with prospective collaborators to identify topics of mutual interest and to suggest a plan of work to address them.
When such a plan of work has been identified, it can be placed on the \gls{SDCP} ``programme board'' and prioritized by the \gls{PMT} in the usual way \parencite{SDC-002}.
The Product Owner is responsible for representing this work within the \gls{PMT}; if appropriate, the (potential) collaborator may also be invited to the \gls{PMT} meeting to provide additional input.

The program backlog and plans for upcoming program increments will be made public, so that the whole community --- certainly including (potential) collaborators --- understand what work is being undertaken and how it relates to their own interests.

The Scrum Master and Product Owner of the development team should agree with the collaborator on a case-by-case basis how they can best engage with the work of the team.
For example, this may include collaborators being invited to observe or participate in team events like daily standups, iteration reviews, and iteration demos.

\subsection{Code Contributions}

Wherever possible, \gls{SDCP} products will be made available as open source software, following the principles in the \sdcCite{dijkema_tammo_jan_2018_3479829}.
External contributions to the source code are welcomed.

All external contributions will be subject to the same standards of engineering, review, and documentation as internally-developed code.
Where necessary, \gls{SDCP} team members will engage with the authors of the contribution to help them achieve the required standard; on occasion, \gls{SDCP} team members may undertake the necessary work themselves if the contribution is identified as sufficiently high value.

Not all external code contributions will be accepted: each will be assessed on its own merits, with acceptance criteria including:

\begin{itemize}

  \item{The extent to which the contribution is aligned with present or likely future \gls{SDC} development goals;}
  \item{The state of the code, and the amount of effort required from \gls{SDCP} staff to integrate it with existing systems;}
  \item{The likely support load which will be imposed on the \gls{SDCP} should this functionality be made available.}

\end{itemize}

If a code contribution is not accepted, \gls{SDCP} staff will communicate the reasoning to the original author of the code.

The \gls{SDCP} will publish documentation describing the process by which external contributors may submit code.
This documentation is not available at the time of writing.

\section{The Public \gls{SDCP} Development Cycle}
\label{sec:public}

As described in \sdcCite{SDC-002}, the \gls{SDCP} has adopted a three-month ``program increment'' to set the cadence for its work.
Broadly, each increment begins by setting goals for the work ahead (the ``PI Planning'' event), and concludes with a ``system demo'' at which the work achieved over the increment is demonstrated.
This cadence naturally lends itself to engaging with community stakeholders.
In particular:

\begin{itemize}

  \item{The results of PI Planning events will be made public, through a to-be-determined medium (e.g. a regular \gls{SDCP} newsletter, or posted on a web page).
        This provides a convenient mechanism by which members of the wider community can understand and engage with the work in progress.}

  \item{At least some part of the system demo event will be open to the community.
        It will be advertised to identified stakeholders, at least including the \gls{LUC} and \gls{LOFAR} \glspl{KSP}.
        Attendees will be invited to provide feedback on the functionality which has been delivered in the program increment.}

\end{itemize}

In addition to the above, a long-term roadmap will be maintained by the \gls{PMT} which provides an outlook on the expected development schedule beyond the next program increment.
Note that, due to the nature of the \gls{SAFe} approach to development, such a roadmap will necessarily be speculative and may not accurately represent the work which is eventually undertaken.

Participation in other \gls{SDCP} events at the development team level may be arranged directly with agreement of the team's Scrum Master (see \cref{sec:tech:organize}).


\clearpage
\printbibliography

\end{document}
